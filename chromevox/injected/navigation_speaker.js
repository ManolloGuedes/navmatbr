// Copyright 2013 Google Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @fileoverview A class for speaking navigation information.
 * @author stoarca@google.com (Sergiu Toarca)
 */


goog.provide('cvox.NavigationSpeaker');

goog.require('cvox.NavDescription');

/**
 * @constructor
 */
cvox.NavigationSpeaker = function() {
  /**
   * This member indicates to this speaker to cancel any pending callbacks.
   * This is needed primarily to support cancelling a chain of callbacks by an
   * outside caller.  There's currently no way to cancel a chain of callbacks in
   * any other way.  Consider removing this if we ever get silence at the tts
   * layer.
   * @type {boolean}
   */
  this.stopReading = false;

  /**
   * An identifier that tracks the calls to speakDescriptionArray. Used to
   * cancel a chain of callbacks that is stale.
   * @type {number}
   * @private
   */
  this.id_ = 1;

  /**
   * Actual read position
   * @type {!cvox.CursorSelection}
   */
  this.currentNode = null;
  this.currentNodePosition = null;
  this.previousDescriptionArray = null;
  this.previousCompletionFunction = null;
  this.previousInitialQueueMode = null;

};

/**
 * Set current node to node passed as parameter
 */
cvox.NavigationSpeaker.prototype.updateCurrentNode = function (node) {
  this.currentNode = node;
}

/**
 * Return node that have been spoken at the moment
 */
cvox.NavigationSpeaker.prototype.getCurrentNode = function() {
  return this.currentNode;
}

/**
 * Set current node position to node passed as parameter
 */
cvox.NavigationSpeaker.prototype.updateCurrentNodePosition = function (position) {
  this.currentNodePosition = position;
}

/**
 * Return node position that have been spoken at the moment
 */
cvox.NavigationSpeaker.prototype.getCurrentNodePosition = function() {
  return this.currentNodePosition;
}

cvox.NavigationSpeaker.prototype.finishSpeakNodeNavigation = function() {
  this.currentNode = null;
  this.currentNodePosition = null;
  this.previousDescriptionArray = null;
  this.previousCompletionFunction = null;
  this.previousInitialQueueMode = null;
}

cvox.NavigationSpeaker.prototype.getSpeakState = function() {
  return {
    position: this.currentNodePosition,
    descriptionArray: this.previousDescriptionArray,
    completionFunction: this.previousCompletionFunction,
    queueMode: this.previousInitialQueueMode
  }
}

/**
 * Speak all of the NavDescriptions in the given array (as returned by
 * getDescription), including playing earcons.
 *
 * @param {Array.<cvox.NavDescription>} descriptionArray The array of
 *     NavDescriptions to speak.
 * @param {number} initialQueueMode The initial queue mode.
 * @param {Function} completionFunction Function to call when finished speaking.
 */
cvox.NavigationSpeaker.prototype.speakDescriptionArray = function(
    descriptionArray, initialQueueMode, completionFunction) {
  descriptionArray = this.reorderAnnotations(descriptionArray);

  this.previousCompletionFunction = completionFunction;
  this.previousDescriptionArray = descriptionArray;
  this.previousInitialQueueMode = initialQueueMode;

  this.speakDescriptionArrayFromPosition(descriptionArray, completionFunction, initialQueueMode, 0);
};


/**
 * Checks for an annotation of a structured elements.
 * @param {string} annon The annotation.
 * @return {boolean} True if annotating a structured element.
 */
cvox.NavigationSpeaker.structuredElement = function(annon) {
  // TODO(dtseng, sorge): This doesn't work for languages other than English.
  switch (annon) {
    case 'table':
    case 'Math':
    return true;
  }
  return false;
};


/**
 * Reorder special annotations for structured elements to be spoken first.
 * @param {Array.<cvox.NavDescription>} descriptionArray The array of
 *     NavDescriptions to speak.
 * @return {Array.<cvox.NavDescription>} The reordered array.
 */
cvox.NavigationSpeaker.prototype.reorderAnnotations = function(
    descriptionArray) {
  var descs = new Array;
  for (var i = 0; i < descriptionArray.length; i++) {
    var descr = descriptionArray[i];
    if (cvox.NavigationSpeaker.structuredElement(descr.annotation)) {
      descs.push(new cvox.NavDescription({
        text: '',
        annotation: descr.annotation
      }));
      descr.annotation = '';
    }
    descs.push(descr);
  }
  return descs;
};

cvox.NavigationSpeaker.prototype.speakDescriptionArrayFromPosition = function(descriptionArray, completionFunction, initialQueueMode, position = 0) {
  
  descriptionArray = descriptionArray || this.previousDescriptionArray;
  completionFunction = completionFunction || this.previousCompletionFunction;
  initialQueueMode = initialQueueMode || this.previousInitialQueueMode;

  console.log('speaking description -> ');
  console.log(descriptionArray);
  this.stopReading = false;
  this.id_ = (this.id_ + 1) % 10000;
  // Using self rather than goog.bind in order to get debug symbols.
  var self = this;
  var speakDescriptionChain = function (i, queueMode, id) {
    var description = descriptionArray[i];
    if (description && description.node) {
      self.updateCurrentNode(description.node);
      self.updateCurrentNodePosition(i);
    }
    //window.console.log(description.speak);
    // if(self.id_ != id) {
    //   self.finishSpeakNodeNavigation();
    //   return;
    // } else 
    if (!description || self.stopReading || self.id_ != id) {
      console.log('stop');
      return;
    }
    var startCallback = function () {
      for (var j = 0; j < description.earcons.length; j++) {
        cvox.ChromeVox.earcons.playEarcon(description.earcons[j]);
      }
    };
    var endCallbackHelper = function () {
      speakDescriptionChain(i + 1, cvox.AbstractTts.QUEUE_MODE_QUEUE, id);
    };
    var endCallback = function () {
      // We process content-script specific properties here for now.
      if (description.personality &&
        description.personality[cvox.AbstractTts.PAUSE] &&
        typeof (description.personality[cvox.AbstractTts.PAUSE]) == 'number') {
        setTimeout(endCallbackHelper, description.personality[cvox.AbstractTts.PAUSE]);
      }
      else {
        endCallbackHelper();
      }
      if ((i == descriptionArray.length - 1) && completionFunction) {
        completionFunction();
      }
    };
    if (!description.isEmpty()) {
      description.speak(queueMode, startCallback, endCallback);
    }
    else {
      startCallback();
      endCallback();
      return;
    }
    if (!cvox.ChromeVox.host.hasTtsCallback()) {
      startCallback();
      endCallback();
    }
  };
  let readPosition = position != 0 ? position : this.getCurrentNodePosition() || 0;
  speakDescriptionChain(readPosition, initialQueueMode, this.id_);
  if ((descriptionArray.length == 0) && completionFunction) {
    completionFunction();
  }
}

